#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# Ücretsiz Postgres Library'si
import psycopg2

# Baglanti kurmak icin
conn = psycopg2.connect("dbname=postgres user=postgres")


# In[ ]:


# Cursor objesi sorgu yollamaya yariyor
cur = conn.cursor()

# Sorgulari gönderirken execute komutu kullaniliyor
cur.execute("DROP TABLE IF EXISTS users")
cur.execute("CREATE TABLE users(id INTEGER PRIMARY KEY, name TEXT, surname TEXT)")


# In[ ]:


# Yeni satir eklemek icin
insert_query1 = "INSERT INTO users VALUES {}".format("(1, 'Oguz', 'Kivircik')")
insert_query2 = "INSERT INTO users VALUES {}".format("(2, 'Okan', 'Ates')")

##cur.execute(insert_query1)
##cur.execute(insert_query2)
    
    
# CSV'den veri alip, db'ye göndermek icin
with open('../dosyalar/users.csv', 'r') as f:
    next(f) # Baslik satirini gecmek icin
    cur.copy_from(f, 'users', sep=';')


# In[ ]:


# Veri Okuma safhasinda:
cur.execute('SELECT * FROM users')

one = cur.fetchone() # 1 tane satir veri almak icin
all = cur.fetchall() # Bütün satirlari almak icin

one


# In[ ]:


# Degisikliklerin hepsini yollamak icin
conn.commit()


# Baglantiyi bitirmek icin
conn.close()

