![Postgres - Python](postgres_python/dosyalar/postgres-py.png)

**Kurulumlar**

1. Anaconda'yi yükle [https://www.anaconda.com/distribution/#download-section][Link]
2. brew install postgres

[Link]: https://www.anaconda.com/distribution/#download-section



**PostgreSQL Server Baslatmak icin**

pg_ctl -D /usr/local/var/postgres start

**Database Olusturmak icin**

initdb /usr/local/var/postgres

**Terminal Bu hatayi verirse**

initdb: directory "/usr/local/var/postgres" exists but is not empty

rm -r /usr/local/var/postgres

**Database Olusturmak icin Tekrar**

initdb /usr/local/var/postgres

createdb users

**User Role Sorunu Olunca**

/usr/local/Cellar/postgresql/<version>/bin/createuser -s postgres